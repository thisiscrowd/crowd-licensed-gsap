# Crowd's Licensed GSAP

Crowd has purchased a ShockinglyGreen GSAP license, which means we get access to
some extra plugins that don't come with GSAP by default.

This repo contains Crowd's licensed version of GSAP for use in our client
projects.

## Installation with NPM

This repo has been setup to allow installation from NPM, just as you would 
install GSAP into your projects normally.

1. Ensure you have NPM installed on your machine
2. `npm install git+ssh://git@bitbucket.org/thisiscrowd/crowd-licensed-gsap.git`
3. The package has been put in your local node_modules folder

## Installation with Bower

This repo has also been setup to allow installation from bower.

1. Ensure you have NPM installed on your machine
2. Install bower using NPM `npm install -g bower`
3. `bower init` and follow the instructions
4. `bower install --save git@bitbucket.org:thisiscrowd/crowd-licensed-gsap.git`
5. The package has been put in your local bower_components folder
